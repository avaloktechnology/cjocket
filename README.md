# README #

```
 ╦╔═╗╔═╗╦╔═╔═╗╔╦╗
 ║║ ║║  ╠╩╗║╣  ║ 
╚╝╚═╝╚═╝╩ ╩╚═╝ ╩ v5.0
```

### Synopsis ###

Jocket is a flexible and easily extensible core for deploying and marshaling servlets written in Ruby.  No Rails required.

I originally built this tool as a helper to make it easier to get [UltraHook](http://www.ultrahook.com) (a service for receiving webhooks on localhost) up and running, but as the project 
went on, it seemed to me that it could do a lot more than that.  Setting up basic custom network communication shouldn't be nearly as hard as it is, and Jocket is an attempt at making it 
much easier.

If nothing else, all of the UltraHook functionality is still available in Jocket, so if you're having trouble getting set up to receive webhooks on localhost, this is the software for you.

### How do I get set up? ###

0) Have Ruby installed.

1) Grab a copy of Jocket ```git clone https://DaveahamLincoln@bitbucket.org/DaveahamLincoln/cjocket.git```.

2) ```gem install bundler``` if you don't have it already.

3) ```cd cjocket``` then ```bundle install``` to install dependencies from the Gemfile.

4) (Optional) [Get an UltraHook API key](http://www.ultrahook.com/register).  You will only need this if you want to use Jocket to receive webhooks forwarded across a NAT ("natpunch mode").

5) Set up your ```.config.yml``` file with ```nano .config.yml```.

```
#API key you generated in step 4.  Enter a random string here if you don't want/need natpunching.
api_key: your-secret-api-key
#The port that you want the Jocket server to run on.
port: 7777

#This is where you configure the endpoints for the Jocket server.  
endpoints:					
  #The "moniker" for the endpoint- just something easy for you to remember to distinguish it from other endpoints.
  SPACE:					
    #The subdomain that you want the endpoint to reside on.  i.e. localhost:7777/thefinalfrontier.
    subdomain: thefinalfrontier			
        #The module that you want to mount to the endpoint (more on that below).
	module: DebugJocket			

#Additional endpoints can be added using the same general format.
# TIME:						
#   subdomain: thefourthdimension
#   module: YourCustomJocket
```

6) Launch Jocket in foreground mode with ```ruby jocket-launcher.rb -f```.  If everything is set up correctly, you should see something like this:

```
 ╦╔═╗╔═╗╦╔═╔═╗╔╦╗
 ║║ ║║  ╠╩╗║╣  ║ 
╚╝╚═╝╚═╝╩ ╩╚═╝ ╩ v5.0b
Loading configuration...
Loading configuration...DONE
[2018-04-19 13:48:01] INFO  WEBrick 1.3.1
[2018-04-19 13:48:01] INFO  ruby 2.3.1 (2016-04-26) [x86_64-linux-gnu]
Staging endpoints...
SPACE:
  Subdomain: thefinalfrontier
  Module: DebugJocket
    Mounting DebugJocket on /thefinalfrontier...
    Mounting DebugJocket on /thefinalfrontier...DONE
Staging endpoints...DONE
Launching the Jocket server on port 7777...
[2018-04-19 13:48:01] INFO  WEBrick::HTTPServer#start: pid=25028 port=7777
```

7) Open up a second terminal window and test the configuration with ```curl -X POST -d hello-jocket! http://localhost:7777/thefinalfrontier```.  You should see something like this appear
in the terminal where Jocket is running:

```
POST /thefinalfrontier HTTP/1.1
User-Agent: curl/7.35.0
Accept: */*
Content-Type: application/x-www-form-urlencoded
Accept-Encoding: gzip;q=1.0,deflate;q=0.6,identity;q=0.3
Connection: close
Host: localhost:7777
Content-Length: 13

hello-jocket!
127.0.0.1 - - [25/Sep/2017:11:17:50 EDT] "POST /thefinalfrontier HTTP/1.1" 200 0
- -> /thefinalfrontier
```

8) If everything's working correctly, shut down Jocket with ```Ctl^C```.

9) (Optional) Hack the planet!

### Startup Options ###

```
-f:    Launch in foreground mode for debugging.
-n:    Launch in NAT Punch mode.  Requires an ultrahook API key.
<no args>:    Launch in default mode.
-h:    Show this screen.
```

### Reference Modules ###

In addition to the core Jocket modules, we've included a few reference modules that can be used to achieve different tasks with Jocket.

```
/modules/broker_jocket.rb
```

A Jocket module that acts as a gateway for other Jocket modules to use when talking with one or more APIs.  There are a few advantages to using a BrokerJocket vs a set of 
discrete client implementations.

0.  You can "alias" complex API operations to simple function names and address them with a simple POST from a Jocket.
1.  API requests made through a BrokerJocket are naturally federated- only operations that you specifically define for a client are available for use.
2.  You can access multiple specially-constrained clients through the same endpoint, which is really powerful.  This allows you to do things like setting up one endpoint to 
    handle user account operations across multiple services, another to handle CRUD across multiple services, etc.
3.  Passing an API request through a BrokerJocket gives you the opportunity to preprocess the response before handing it back to the requesting endpoint.

```
/modules/assignment_jocket.rb
```

A Jocket module that utilizes a BrokerJocket connected to a JIRA site to automatically assign an issue to a user based on the attributes of the issue at the time the webhook 
was triggered.

This is included largely as an illustrative example of how Jocket can be used to extend the set of behaviors available to an existing application without having to modify
the application itself.  On its own, it is very difficult to make JIRA capable of assigning an issue automatically based on multiple factors (i.e. transition name AND 
custom field value) vs a single factor (i.e. component name).  

With Jocket, it's comparatively simple to add this complex behavior.  The 'behind the scenes' process looks something like this:

0. A transition is executed in JIRA that sends a webhook to an AssignmentJocket running on a Jocket endpoint.
1. The AssignmentJocket parses the data contained within the webhook request and extracts the information needed to retrieve the issue record through the JIRA API.
2. The AssignmentJocket sends a request to a BrokerJocket, asking the broker to retrieve the issue record.
3. The BrokerJocket retrieves the issue record and sends it back to the AssignmentJocket.
4. The AssignmentJocket examines the issue record and compares it to an internal schema to determine if an automatic assignment operation is necessary for the given event.
5. If so, the AssignmentJocket sends a request to the BrokerJocket to retrieve the user record for the assignee defined in the internal assignment schema.
6. The BrokerJocket retrieves the user record and sends it back to the AssignmentJocket.
7. The AssignmentJocket staples the issue record and user record for the new assignee together, and sends a request to the BrokerJocket to update the issue with the new assignee.
8. The BrokerJocket makes the requested change and notifies the AssignmentJocket that the change has been made.
9. The AssignmentJocket returns 200 OK and starts waiting for new requests.

```
/modules/psypher_jocket.rb
```

A Jocket module that will generate 4-word strings in three languages and return them as plaintext.

This module is included to show that Jocket can be used to "serve" scripts over HTTP.  Every time the endpoint is hit, the script executes and the output is displayed on the page.
The data can also be accessed programatically via ```curl``` or similar.

This has all sorts of interesting applications.  For instance- if you don't want to have to SSH into a box and run 

```ps aux | grep foo```

all the time, but you find yourself doing it often, you could set up a similar Jocket module that accepted requests of the form

```
{
    search_term: "foo"
}
```

on the /psaux endpoint and responded with the output of 

```system("ps aux | grep #{request['search_term']}")```

Obviously this has a crazy potential for misuse, but if properly secured with a proxy server or similar could provide a very powerful tool for sysadmins.

### Writing Custom Modules ###

Jocket makes it very easy to extend the core functionality with your own custom modules.  Many modules (mostly for JIRA) have been included in a generic commented form as examples of what you can do.

0) ```cd cjocket/modules```.  The modules directory is where all of Jocket's modules live.  They are loaded into the environment automatically at runtime.

1) Make a copy of the Jocket template to start from with ```cp template_jocket.rb your_custom_jocket.rb```.  By convention, all Jocket modules are named [module_name]_jocket.rb.

2) Open up the copy you made with ```nano your_custom_jocket.rb```.  It should look like this:

```
#Template for developing your own custom Jocket modules.

require_relative 'jocket.rb'            #Requires the Jocket class.

class YourCustomJocket < Jocket         #All Jocket modules inherit from the main Jocket class.
    def self.moniker                    #A required method that allows your class to identify itself when called in the abstract.
      "YourCustomJocket"                # Returns the string entered here.
    end

    def do_GET(request,response)        #Basic listener for GET requests.
      puts request
      response.status = 200
    end
  
    def do_POST(request,response)       #Basic listener for POST requests.
      puts request
      response.status = 200
    end
  end
```

3) Write your custom code.

4) ```nano cjocket/.config.yml``` to edit your config file again.

```
api_key: your-secret-api-key
port: 7777

endpoints:
  SPACE:
    subdomain: thefinalfrontier
	module: DebugJocket

 TIME:                                      
   subdomain: thefourthdimension
   module: YourCustomJocket                 #<-- To load a custom module, all that's required is to put the name of the class contained within the module here.
```

5) ```ruby jocket-launcher.rb``` to make sure that everything loads correctly.

6) The rest is up to you!

### Contribution guidelines ###

Feel free to submit anything you find useful.

### License ###

Jocket servlet core
Copyright (C) 2017

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
