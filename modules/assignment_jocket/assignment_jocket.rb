 #     # ####### #     # ######     #     # ###  #####   #####  ### ####### #     # 
  #   #  #     # #     # #     #    ##   ##  #  #     # #     #  #  #     # ##    # 
   # #   #     # #     # #     #    # # # #  #  #       #        #  #     # # #   # 
    #    #     # #     # ######     #  #  #  #   #####   #####   #  #     # #  #  # 
    #    #     # #     # #   #      #     #  #        #       #  #  #     # #   # # 
    #    #     # #     # #    #     #     #  #  #     # #     #  #  #     # #    ## 
    #    #######  #####  #     #    #     # ###  #####   #####  ### ####### #     # [ ] [ ] [ ]

	#	...should you choose to accept it...
	#	
	#	...is to go attempt to finish documenting this code. 
	#
	#	Don't think of it as code- think of it as a logic puzzle.  I've left many hints below in several forms to help you find your way around.
	#
	#	I'm not concerned with whether you're right or wrong- I'm interested to see how far you can get if you treat this as a very complex crossword puzzle.
	#

  ######  #     # #       #######  #####  
  #     # #     # #       #       #     # 
  #     # #     # #       #       #         [ ]
  ######  #     # #       #####    #####  
  #   #   #     # #       #             # 
  #    #  #     # #       #       #     #   [ ]
  #     #  #####  ####### #######  #####  

	#
	#	1. Send a copy of your work to me every couple hours or so so I can look over what you've done so far.
	#
	#	2. Feel free to use any resources you would like to help you with this task.
	#	  	2b. Searching '<UNKNOWN TERM> ruby' in Google will probably get you good results if you get stuck.
	#
	#	3. Points are given arbitrarily, but scoring is based on quantity of edits and your line of thinking, not your level of correctness.
	#		3b. Seriously.  I would rather you just brainstorm all over this file and be 100% wrong than have 1 theory that's perfectly correct.
	#
	#	4. Please use proper commenting conventions!  Makes it easier for me to see your edits.
	#
	#	5. It's late for me, so I may not be really 'on' until the afternoon, but I will read your edits and touch base with you when I become available.
	#
	#	*. Bonus ironman mode points may be awarded for editing the file using the nano text editor on linux.  It's weird at first, but all the shortcuts are listed at the bottom.
	#	
	#		The mouse won't help you here.
	#
	#		^ means "CTL"
	#		M means "ALT"
	#
	#		Useful shortcuts:
	#			CTL+O, then ENTER == Save
	#			CTL+K == Cut
	#			CTL+U == Paste
	#		
	#	
	#		To open a file in nano, just run
	#			>nano <filename>
	#

  #####  ####### ####### ######     #       #     #  #####  #    #                          
 #     # #     # #     # #     #    #       #     # #     # #   #                           
 #       #     # #     # #     #    #       #     # #       #  #                            
 #  #### #     # #     # #     #    #       #     # #       ###                             
 #     # #     # #     # #     #    #       #     # #       #  #                            
 #     # #     # #     # #     #    #       #     # #     # #   #                           
  #####  ####### ####### ######     #######  #####   #####  #    #                          
                                                                                            
  #####  ######     #     #####  #######     #####  ####### #     # ######  ####### #     # 
 #     # #     #   # #   #     # #          #     # #     # #  #  # #     # #     #  #   #  
 #       #     #  #   #  #       #          #       #     # #  #  # #     # #     #   # #   
  #####  ######  #     # #       #####      #       #     # #  #  # ######  #     #    #    
       # #       ####### #       #          #       #     # #  #  # #     # #     #    #    
 #     # #       #     # #     # #          #     # #     # #  #  # #     # #     #    #    
  #####  #       #     #  #####  #######     #####  #######  ## ##  ######  #######    #     [ ]


#############################
###>>>FILE BEGINS HERE<<<####
#############################

###assignment_jocket.rb
#
#  Description: Jocket for reassigning issues based on transition-linked webhooks sent by JIRA.
#		Essentially allows us to tell JIRA who to assign an issue to based on the data contained within the issue.
#
#  Instructions: Within JIRA, a webhook needs to be set up to point to this jocket endpoint
#    The webhook must then be enabled on the appropriate workflow steps as a post-action
#
###

###
#
#  Requirements
#
###

require 'json'				#	Allows us to encode calls to the broker as JSON
require_relative '../jocket.rb'		#	Requires the Jocket class.

###
#
#  Global declarations
#
###

class AssignmentJocket < Jocket		#	All Jocket modules inherit from the main Jocket class.
	def self.moniker                #	A required method that allows your class to identify itself when called in the abstract.
		"AssignmentJocket"	#	Returns the string entered here.
	end

	#	The config method returns a hash containing the data used to drive this Jocket's function.
	#
	#	This is the general layout:
	#	
	#	:schema => {
	#	  Contains the mapping schema used to make assignment decisions for issues.
	#
	#		<PROJECT SLUG> => {
	#		  Contains the mapping schema for a given project
	#
	#			<TRANSITION> => {
	#			  Contains the mapping schema for a given workflow transition
	#
	#				:eval => "<STATEMENT>",
	#				  A statement that when evaluated as code returns a string matching a key in the <TRANSITION> hash
	#
	#				"<KEYS> => "some strings"
	#				  Key(s) that can be returned by eval(:eval), mapped to values that can be used as an input in a broker_transaction
	#			}
	#		}
	#	},
	#
	#	:broker_requests => {
	#	  Contains a listing of operations available for federation through the BrokerJocket.  
	#	  Think of these like "API Aliases."  i.e. :get_user contains the data necessary to process a GET_USER request through the API bridge provided by the BrokerJocket.
	#
	#		:<OPERATION ALIAS> => {
	#		  By convention, this should have a matching gen_<OPERATION ALIAS> method in the main Jocket body.  This contains the evaluation statements and template necessary to generate a request.
	#
	#			:evals => [
	#			  A list of operations that, when executed as code, modify the contents of the :payload key by executing the <STATEMENTS> for each <NAME>.
	#			  These statements are evaluated as a group, not individually.
	#			  Individual statements should be contained within :eval keys as in the :schema[<PROJECT SLUG>][<TRANSITION>] section above.
	#			],
	#
	#			:payload => {
	#			  The data necessary to issue a request to a broker that executes a federated transaction.  Think of these as "arguments" being passed to a Broker-side function.
	#
	#				#########################################
	#				#	THIS GROUPING IS STANDARD	#
	#				#########################################
	#				"requester" => "@mountpoint",	#	All Jocket modules store their configured mountpoints to @mountpoint at load time so that they can communicate internally.
	#				"service" => "jira_client",	#	The name of a Broker-side service, usually an API client of some kind.
	#				"operation"=> "get_issue",	#	The name of a federated operation offered by the selected service.
	#				#########################################
	#				
	#				"parameters" => {
	#				  The set of parameters being passed to the selected operation.
	#
	#					"<NAME>" => "<STATEMENT>",
	#					  <NAME> is the name of the parameter expected by the operation.
	#
	#					  <STATEMENT> is a statement that when evaluated as code will return a string that can be passed directly in an API call.
	#					    This is the statement that the operations contained within :evals will execute.
	#
	#					    Once it has been executed, the associated :eval statement will proceed to assign the return value of <STATEMENT> to <NAME>
	#				}				
	#			}
	#		},
	#
	#		"//Comments can be added within the config hash in this format if necessary." => "",
	#
	#	}
	#

        def config
                {	
			"//This block stores the URL:PORT for the broker used to make API requests to JIRA." => "",
			:broker => "http://localhost:7777/jira_broker",
			"//This block stores the actual schema used to make assignment decisions" => "",
                        :schema => {
				"//Project slugs go at this level." => "",
                                "EXAMPLE" => {
					"//Workflow names go at this level." => "",
					"EXAMPLE: Workflow with Jocket Hooks" => {
						"//Transition names go at this level.  Ensure that the transition is set to trigger a webhook pointed at this endpoint." => "",
	                                        "TRANSITION1" => {
							"//:eval is a statement that when evaluated as code will return a string matching a key in the associated <TRANSITION> hash." => "",
							"//	In this case, eval('user') returns 'user', so when TRANSITION1 is evaluated by gen_get_user(), the module will perform this call:" => "",
							"//	config[:schema]['EXAMPLE']['EXAMPLE: Workflow with Jocket Hooks']['TRANSITION1']['user']." => "",
        	                                        :eval => "'user'",
							"//	The result is 'username'." => "",
                	                                "user" => "username"
                        	                },

        	                                "TRANSITION2" => {
							"//This is an example of a more complicated :eval statement, used to determine the assignee for a transition based on the value of a custom field." => "",
							"//	As long as the referenced values have been set by the time gen_get_user() is called, this allows the hash to return dynamically calculated" => "",
							"//	values.  This is useful because it enables branching decision behavior along explicitly whitelisted routes, without needing to worry about" => "",
							"//	malicious requests, due to the fact that unmapped values are naturally discarded as invalid requests by the server." => "",
							:eval => "info['issue']['fields']['customfield_10800']['value']",
                                	                "CUSTOMVALUE1" => "username",
							"CUSTOMVALUE2" => "username2"                                        	
						},

	                                },

                                        "EXAMPLE: Second Workflow with Jocket Hooks" => {
	                                        "COO Approves" => {
        	                                        :eval => "'user'",
                	                                "user" => "username3"
						}
					}
				}
                        },

			"//This block stores mappings to broker operations defined in modules/broker_jocket/broker_jocket.rb" => "",
                        :broker_requests => {

				"//This block contains the instructions needed to allow modules to request the issue data for a given issue_id." => "",
                                :get_issue => {
					"//These eval statements are used to 'fill in' the dynamically calculated values from the :payload template" => "",

					"//When evaluated by gen_get_issue(), this statement sets the value of the broker_request[:payload]['requester'] key to eval(@mountpoint), which refers to" => "",
					"//	the mountpoint where this instance of the module resides." => "",
                                        "broker_request[:payload]['requester'] = eval(broker_request[:payload]['requester'])" => "",
					"//When evaluated by gen_get_issue(), this statement sets the value of the broker_request[:payload]['parameters']['issue_id'] key to" => "",
					"//	eval(info['issue']['id'])" => "",

                                        :evals => [
                                                "broker_request[:payload]['parameters']['issue_id'] = eval(broker_request[:payload]['parameters']['issue_id'])"
                                        ],

					"//This block contains a template for the parameters required by the target broker operation." => "",
					"//	Whenever a request is being served, the relevant gen_get_x() methods create a copy of the associated :broker_request block, including the :payload" => "",
					"//	When executed, the :evals block destructively modifies the values in the :payload key contained within the copy, but the template data remains intact in" => "",
					"//	as part of self.config." => "",
                                        :payload => {
                                                "requester" => "@mountpoint",
						"//This maps to a service name (usually a client) in the :do_POST[:services] block of modules/broker_jocket/broker_jocket.rb's config method." => "",
                                                "service" => "jira_client",
						"//This maps to an operation name under the above key in the :do_POST block of modules/broker_jocket/broker_jocket.rb's config method." => "",
                                                "operation"=> "get_issue",
                                                "parameters" => {
                                                        "issue_id" => "info['issue']['id']"
                                                }
                                        }
                                },

				"//This block contains the instructions needed to allow modules to request the user data for a given username." => "",
                                :get_user => {

                                        :evals => [
                                                "broker_request[:payload]['requester'] = eval(broker_request[:payload]['requester'])",
                                                "broker_request[:payload]['parameters']['user'] = eval(broker_request[:payload]['parameters']['user'])"
                                        ],

                                        :payload => {
                                                "requester" => "@mountpoint",
                                                "service" => "jira_client",
                                                "operation"=> "get_user",
                                                "parameters" => {
                                                        "user" => "self.schema[slug][workflow][transition][eval(self.schema[slug][workflow][transition][:eval])]"
                                                }
                                        }
                                },

				"//This block contains the instructions needed to allow modules to request a change in user assignment." => "",
                                :assign_user => {

                                        :evals => [
                                                "broker_request[:payload]['requester'] = eval(broker_request[:payload]['requester'])",
                                                "broker_request[:payload]['parameters']['issue_id'] = eval(broker_request[:payload]['parameters']['issue_id'])",
                                                "broker_request[:payload]['parameters']['fields']['assignee']['name'] = eval(broker_request[:payload]['parameters']['fields']['assignee']['name'])"
                                        ],

                                        :payload => {
                                                "requester" => "@mountpoint",
                                                "service" => "jira_client",
                                                "operation"=> "save_issue",
                                                "parameters" => {
                                                        "issue_id" => "info['issue']['id']",
                                                        "fields" => {
                                                                "assignee" => {
                                                                        "name"=>"user['name']"
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }

        end

	def schema
		
		#Returns the :schema from self.config
		self.config[:schema]	

	end

	def broker_poll(mountpoint,broker_request)
	#	Polls a BrokerJocket running on localhost and returns the response
		JSON.parse!(`curl -H 'Content-Type: application/json' -d '#{broker_request}' #{mountpoint}`)
	end

	def gen_get_issue(info)
		#Builds a broker_request to retrieve an issue object.

		broker_request = self.config[:broker_requests][:get_issue]

                #       Evaluate :eval statements
                #       This is necessary to allow the hash to dynamically calculate key values within the payload
                broker_request[:evals].each do |x|
                        eval(x)
                end

		JSON.generate(broker_request[:payload])
	end

	def gen_get_user(info,slug,transition,workflow)

		#Builds a broker request to retrieve a user object.

		broker_request = self.config[:broker_requests][:get_user]

                #       Evaluate :eval statements
                #       This is necessary to allow the hash to dynamically calculate key values within the payload
                broker_request[:evals].each do |x|
                        eval(x)
                end

		JSON.generate(broker_request[:payload])

	end

	def gen_assign_user(info,user,workflow)
		#Builds a broker request to assign an issue to a user.

                broker_request = self.config[:broker_requests][:assign_user]

                #       Evaluate :eval statements
                #       This is necessary to allow the hash to dynamically calculate key values within the payload
                broker_request[:evals].each do |x|
                        eval(x)
                end

                JSON.generate(broker_request[:payload])
	end  

	def do_GET(request,response)        #Basic listener for GET requests.
		puts request
		response.status = 200
	end

	def do_POST(request,response)       #Basic listener for POST requests.
		puts "::TRUST::ASSIGNMENT_JOCKET::WEBHOOK RECEIVED"

		info = JSON.parse!(request.body)
		slug = info["issue"]["fields"]["project"]["key"]
		workflow = info["transition"]["workflowName"]
		transition = info["transition"]["transitionName"]

		###DEBUG###
#		d_fields = info["issue"]["fields"]
#		d_customfield = info["issue"]["fields"]["customfield_10800"]
#		d_value = info["issue"]["fields"]["customfield_10800"]["value"]

		puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST..."

		broker_request = gen_get_issue(info)

		puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST...DONE"	

		puts "::TRUST::ASSIGNMENT_JOCKET::FORWARDING REQUEST TO BROKER..."
#		puts broker_request

		#	Sends the request for information on the issue ID to the broker and saves the parsed JSON response to a variable
		issue = broker_poll("#{self.config[:broker]}",broker_request)

		puts "::TRUST::ASSIGNMENT_JOCKET::FORWARDING DATA TO BROKER...DONE"

		puts "::TRUST::ASSIGNMENT_JOCKET::RETRIEVED PAYLOAD FROM BROKER"

		puts "::TRUST::ASSIGNMENT_JOCKET::CHECKING SCHEMA FOR REASSIGNMENT OPERATION..."

		#	Validates that a mapping exists within the schema
#		if self.schema.keys.include?(slug) && self.schema[slug].keys.include?(transition)
                if self.schema.keys.include?(slug) && self.schema[slug].keys.include?(workflow) && self.schema[slug][workflow].keys.include?(transition)
			puts "::TRUST::ASSIGNMENT_JOCKET::CHECKING SCHEMA FOR REASSIGNMENT OPERATION...TRUE"

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST..."	

			#	Generate a broker request for the user data

			broker_request = gen_get_user(info,slug,transition,workflow)

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST...DONE"	
			puts broker_request

			#	Sends the request for information on the user to the broker and saves the parsed JSON response to a variable
			user = broker_poll("#{self.config[:broker]}",broker_request)
			
			puts "::TRUST::ASSIGNMENT_JOCKET::RETRIEVED PAYLOAD FROM BROKER"

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST..."	

			broker_request = gen_assign_user(info,user,workflow)

			puts "::TRUST::ASSIGNMENT_JOCKET::GENERATING BROKER REQUEST...DONE"	
			puts broker_request

                    	#       Sends the request for an issue update to the broker and saves the response to a variable.
#                        update = broker_poll("http://localhost:7777/jira_broker",broker_request)
                        update = broker_poll("#{self.config[:broker]}",broker_request)
                        puts "::TRUST::ASSIGNMENT_JOCKET::RETRIEVED PAYLOAD FROM BROKER"
			puts update
			
			puts "::TRUST::ASSIGNMENT_JOCKET::REASSIGNMENT FROM #{issue["fields"]["assignee"]["name"]} => #{user["name"]} COMPLETE."

		else
			#Fails gracefully
			puts "::TRUST::ASSIGNMENT_JOCKET::CHECKING SCHEMA FOR REASSIGNMENT OPERATION...FALSE"
			response.status = 200

		end				

		response.status = 200
	end
end
