#Debugging module
#
#Prints the contents of GET and POST requests to the terminal, and returns a 200 response.

require_relative '../jocket.rb'

class DebugJocket < Jocket
    def self.moniker
      "DebugJocket"
    end

    def do_GET(request, response)
      puts request
      response.status = 200
    end
  
    def do_POST(request, response)
      puts request
      response.status = 200
    end
  end
