#Attaches statically served documents to JIRA issues on-demand.
#
#

require_relative '../jocket.rb'            #Requires the Jocket class.

class AttachmeJocket < Jocket         #All Jocket modules inherit from the main Jocket class.
	def self.moniker              #A required method that allows your class to identify itself when called in the abstract.
		"AttachmeJocket"      # Returns the string entered here.
	end

	def config
		{	
			"//This is where you define the list of files that this endpoint can serve." => "",
			:resources => {
				:tracking_sheet => "/path-to/jocket/modules/attachme_jocket/tracking_sheet.txt"
			},

			"//This is a lightweight JIRA client definition that can be used without needing to set up a binding in modules/broker_jocket/broker_jocket.rb." => "",
			:jira_client => {
				:username => "user",
		                :password => "pass",
                		:site     => 'https://jira.yourcorp.org:PORT/rest/api/2/issue/',
			}
		}
	end

	def do_GET(request,response)        #Basic listener for GET requests.

		puts request
		response.status = 200
	end
  
	
	#This method expects JSON in the following format:
	#
	#	{"id":"{{issue.key}}","resource":"tracking_sheet"}
	#
	def do_POST(request,response)       #Basic listener for POST requests.
		info = JSON.parse!(request.body)
#		puts info
		puts "::TRUST::ATTACHME::PROCESS_ISSUE::PARSING INFO HASH..."
		issue_id = info['id']
		#Skips processing when an nonexistent resource is requested.
		unless !self.config[:resources].keys.include?(info["resource"])
			file = self.config[:resources[info["resource"]]]
			puts "::TRUST::ATTACHME::PROCESS_ISSUE::PARSING INFO HASH...DONE"
			puts "\n\nParser output:\n\nIssue: #{issue_id}\n\nResource: #{file}\n\n"
			puts "::TRUST::ATTACHME::PROCESS_ISSUE::GENERATING CLIENT UPLOAD..."
        	        config = self.config[:jira_client]
			puts "\n::TRUST::ATTACHME::PROCESS_ISSUE::GENERATING CLIENT UPLOAD...DONE"
			#Generates the target upload url
			url = "#{config[:site]}" + "#{issue_id}/attachments"
			#Crafts and executes the required POST request using `curl`
			system("curl -D- -u #{config[:username]}:#{config[:password]} -X POST -H 'X-Atlassian-Token: no-check' -F \"file=@#{file}\" #{url}")
		else
			puts "::TRUST::ATTACHME::SKIP:: REQUESTED RESOURCE DOES NOT EXIST.  SKIPPING."
		end
	end

end
